// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ngCordovaBeacon', 'angular.filter','starter.controllers','starter.factories','ngStorage','ngCordova','google.places','ngSanitize'])

.run(function($ionicPlatform,$rootScope,$http,$state, $ionicHistory,$cordovaBeacon) {
	
	$rootScope.Host = 'http://tapper.co.il/zode/php/';
	$rootScope.SettingsArray = [];
	$rootScope.currState = $state;
	$rootScope.State = '';
	$rootScope.searchText = "";
	$rootScope.searchJson = [];
	$rootScope.Scans = [];
	$rootScope.CurrentJson = [];
	$rootScope.BeaconOnce = 0;
    $rootScope.beacons = {};
	$rootScope.skip = false; 	
	
	// get settings (about)
	$http.get($rootScope.Host+'/getSettings.php').success(function(data)
	{
		$rootScope.SettingsArray = data;	
		//console.log('SettingsArray: ',$rootScope.SettingsArray);
	});	

	
	/****************************************************/
	/****************  Exit App Function ****************/
	/****************************************************/
	//console.log($state.current.name )
	
	$rootScope.$watch('currState.current.name', function(newValue, oldValue) {
      $rootScope.State = newValue;
    });  
	

	$ionicPlatform.registerBackButtonAction(function (event) 
	{
		if($rootScope.State == 'app.main') 
		{
			 confirmbox = confirm("האם לצאת מהאפליקציה?");
			 if (confirmbox)
				navigator.app.exitApp();
			 else
			    event.preventDefault();
		}
		else
		{
			 $ionicHistory.goBack();
		}
	},100);
	
	
	/*
	$rootScope.pusher = new Pusher('43b1acc0adc60163c8ec', {
			  encrypted: false
	});	
	*/
	$rootScope.CodesArray = [];
	$rootScope.GroupsArray = [];
	$rootScope.TypeSelected = 2;
	$rootScope.UnreadJson = [];
	$rootScope.thirdPartyApp = [];

	
	
  $ionicPlatform.ready(function() {
	  
	  

	
	//cordova.plugins.notification.badge.configure({ autoClear: true });
	//cordova.plugins.notification.badge.set(41);


	
	  
 var notificationOpenedCallback = function(jsonData) {
	  //alert (JSON.stringify(jsonData));
	  //alert (jsonData.additionalData.type);
	  
	  if (jsonData.additionalData.type == "newcomment")
	  {
		   $rootScope.$broadcast('newcomment',jsonData.additionalData);
		   $rootScope.$broadcast('newmessage',jsonData.additionalData);
		   $rootScope.$broadcast('refreshcodes',jsonData.additionalData);	
		   
		   
		   if (jsonData.additionalData.codeid)
		   {
			   if (!jsonData.isActive)
			   {
				   window.location ="#/app/details/"+jsonData.additionalData.codeid;
				   
				   $rootScope.$broadcast('updatebadgecount',jsonData.additionalData);		
			   }
			   
		   }
	  }

	  
	  if (jsonData.additionalData.type == "sharedcode")
	  {
		   $rootScope.$broadcast('sharedcode',jsonData.additionalData);
		   
		   if (jsonData.additionalData.codeid)
		   {
			   if (!jsonData.isActive)
			   {
				   window.location ="#/app/details/"+jsonData.additionalData.codeid;
			   }
			   
		   }
		   
	  }	 	  


	  if (jsonData.additionalData.type == "updatedcode")
	  {
		   $rootScope.$broadcast('updatedcode',jsonData.additionalData);
		   
		   if (jsonData.additionalData.codeid)
		   {
			   if (!jsonData.isActive)
			   {
				   window.location ="#/app/details/"+jsonData.additionalData.codeid;
			   }
			   
		   }

		   
	  }	 

	  
	  
	  if (jsonData.additionalData.type == "deletedcode")
	  {
		   $rootScope.$broadcast('deletedcode',jsonData.additionalData);
	  }	 


	  
    console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
  };

  window.plugins.OneSignal.init("1b13c894-4bfd-4a40-991d-0a351d021a49",
                                 {googleProjectNumber: "293952027061"},
                                 notificationOpenedCallback);

  window.plugins.OneSignal.getIds(function(ids) {
	
  $rootScope.pushId = ids.userId;	
  
  
  
  
  
  if ($rootScope.pushId)
  {
	  /*
	  if ($localStorage.userid =="")
	  {
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			send_params = 
			{
				"user" : $localStorage.userid,
				"pushId" : $rootScope.pushId
				
			}
	

			$http.post($rootScope.Host+'/save_visitor.php',send_params)
			.success(function(data, status, headers, config)
			{

			})
			.error(function(data, status, headers, config)
			{

			});			  
	  }
	 
	 */

  }

	  
  
  //$scope.pushId = ids.userId;
	
	
   //alert (ids.userId)
   //alert (ids.pushToken);

				
  //console.log('getIds: ' + JSON.stringify(ids));
});

								 
  // Show an alert box if a notification comes in when the user is in your app.
  window.plugins.OneSignal.enableInAppAlertNotification(false);
  window.plugins.OneSignal.clearOneSignalNotifications();


  
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
	
	





 
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })


    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })

    .state('app.register', {
      url: '/register',
      views: {
        'menuContent': {
          templateUrl: 'templates/register.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })	
	
    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })	
    .state('app.codes', {
      url: '/codes',
      views: {
        'menuContent': {
          templateUrl: 'templates/codes.html',
          controller: 'CodesCtrl'
        }
      }
    })	

    .state('app.addcode', {
      url: '/addcode/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/addcode.html',
          controller: 'AddCodeCtrl'
        }
      }
    })	

    .state('app.details', {
      url: '/details/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/details.html',
          controller: 'DetailsCtrl'
        }
      }
    })	

    .state('app.groups', {
      url: '/groups',
      views: {
        'menuContent': {
          templateUrl: 'templates/groups.html',
          controller: 'GroupsCtrl'
        }
      }
    })	


    .state('app.addgroup', {
      url: '/addgroup/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/addgroup.html',
          controller: 'AddGroupCtrl'
        }
      }
    })	


    .state('app.forgot', {
      url: '/forgot',
      views: {
        'menuContent': {
          templateUrl: 'templates/forgot.html',
          controller: 'ForgotCtrl'
        }
      }
    })

    .state('app.whatsnew', {
      url: '/whatsnew',
      views: {
        'menuContent': {
          templateUrl: 'templates/whatsnew.html',
          controller: 'WhatsNewCtrl'
        }
      }
    })	
    .state('app.updateinfo', {
      url: '/updateinfo',
      views: {
        'menuContent': {
          templateUrl: 'templates/updateinfo.html',
          controller: 'updateInfoCtrl'
        }
      }
    })	
    .state('app.about', {
      url: '/about',
      views: {
        'menuContent': {
          templateUrl: 'templates/about.html',
          controller: 'AboutCtrl'
        }
      }
    })	

    .state('app.search', {
      url: '/search',
      views: {
        'menuContent': {
          templateUrl: 'templates/search.html',
          controller: 'SearchCtrl'
        }
      }
    })	
	
;
	
	
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
