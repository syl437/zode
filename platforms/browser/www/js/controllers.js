angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope,$state,$ionicPlatform,$cordovaBeacon, $ionicModal, $timeout,$localStorage,$ionicPopup,$http,$rootScope,$cordovaContacts,$ionicActionSheet) 
{
	
  $scope.whatsNewCount = 0;
  $scope.qrCountBadge  =   0;
  $scope.groupsCount = 0;
  
    $scope.didStartMonitoringForRegionLog = '';
    $scope.didDetermineStateForRegionLog = '';
    $scope.didRangeBeaconsInRegionLog = '';
  
		$scope.beacons = {};
		
			

		$rootScope.$watch('BeaconOnce', function () 
		{   
		
			if ($rootScope.BeaconOnce > 0)
			{
				
				if (!$rootScope.skip) {
					window.location.href = "#/app/details/80";
				}
			}

		});


		
	 if (window.cordova) 
	 {
		 
		$scope.$on('$ionicView.enter', function(e) 
		{
		//$ionicPlatform.ready(function() {
	 
			$cordovaBeacon.requestWhenInUseAuthorization();
			
			$rootScope.$on("$cordovaBeacon:didRangeBeaconsInRegion", function(event, pluginResult) {
				var uniqueBeaconKey;
				
				$rootScope.BeaconOnce = pluginResult.beacons.length;
			
			
				if (pluginResult.beacons.length > 0)
				{

				}
				for(var i = 0; i < pluginResult.beacons.length; i++) {
					uniqueBeaconKey = pluginResult.beacons[i].uuid + ":" + pluginResult.beacons[i].major + ":" + pluginResult.beacons[i].minor;
					$scope.beacons[uniqueBeaconKey] = pluginResult.beacons[i];
				}
				
				//alert ($scope.beacons.length)
				
				
				$scope.$apply();
			});
	 
			$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("jaalee", "c974306b-abbf-44f5-ada9-0348ef6ee4b8"));
	 
		});	
		
	
    $rootScope.$on("$cordovaBeacon:didStartMonitoringForRegion", function (event, pluginResult) {
      $scope.didStartMonitoringForRegionLog += '-----' + '\n';
      $scope.didStartMonitoringForRegionLog += JSON.stringify(pluginResult) + '\n';
	  
	  //alert (JSON.stringify(pluginResult));
    });

    $rootScope.$on("$cordovaBeacon:didDetermineStateForRegion", function (event, pluginResult) {
      $scope.didDetermineStateForRegionLog += '-----' + '\n';
      $scope.didDetermineStateForRegionLog += JSON.stringify(pluginResult) + '\n';
    });

    $rootScope.$on("$cordovaBeacon:didRangeBeaconsInRegion", function (event, pluginResult) {
      $scope.didRangeBeaconsInRegionLog += '-----' + '\n';
      $scope.didRangeBeaconsInRegionLog += JSON.stringify(pluginResult) + '\n';
    });

	
		
	  }
			
	
		

	$rootScope.$on('updateBadges', function(event, args) 
	{	
		 $timeout(function() 
		 {
			 if (args)
			 {			 
				 $scope.groupsCount = args[0].groups;
				 $scope.qrCountBadge = args[0].usercodes;	 
			 }			 
			 else
			 {
				 $scope.qrCountBadge++;
			 }
			
		}, 300);
		
	});
 
	$rootScope.$on('unreadwhatsnew', function(event, args) 
	{
		
		 $timeout(function() 
		 {
			if (args == "new")
			{
				$scope.whatsNewCount++;
			}
			else
			{
				$scope.whatsNewCount = args;
			}
		}, 300);
		
	});	

  $scope.navigateWhatsNew = function()
  {
	  if ($scope.whatsNewCount > 0)
	  {
		  window.location.href = "#/app/whatsnew";	
	  }
	  else
	  {
			$ionicPopup.alert({
			title: 'אין הודעות כרגע יש לנסות מאוחר יותר',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			  
	  }
  }
  
  
  $scope.LogOut = function()
  {
				
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לבצע יציאה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			send_params = 
			{
				"user" : $localStorage.userid,
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/logout.php',send_params)
			.success(function(data, status, headers, config)
			{
			  $localStorage.userid = '';
			  $localStorage.facebookid = '';
			  $localStorage.name = '';
			  $localStorage.email = '';
			  $localStorage.phone = '';
			  $localStorage.gender = '';
			  
			$localStorage.birth = '';
			$localStorage.gender = '';
			$localStorage.address = '';

					
			  $localStorage.image = '';
			  window.location.href = "#/app/login";	

			})
			.error(function(data, status, headers, config)
			{

			});								
		} 
		 else 
		 {
		 }
	   });


		
	/*
	  if ($localStorage.facebookid)
	  {
        facebookConnectPlugin.logout(function(){
        },
        function(fail){
        });		  
	  }
	*/

				
  }
})



.controller('LoginRegisterCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$state,$q,$ionicLoading,dateFilter) {

	if ($localStorage.userid)
	{
		$state.go('app.main');
	}




	$scope.register = 
	{
		"name" : "",
		"phone" : "",
		"email" : "",
		"birth" : "",
		"gender" : "0",
		"address" : "",
		"password" : ""
	}
	$scope.login = 
	{
		"email" : "",
		"password" : ""	
	}
	

	//login 
	$scope.LoginBtn = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		var emailRegex = /\S+@\S+\.\S+/;
		
		if ($scope.login.email =="")
		{
			$ionicPopup.alert({
			title: 'יש למלא כתובת דוא"ל',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else if (emailRegex.test($scope.login.email) == false)
		{
			$ionicPopup.alert({
			title: 'כתובת דוא"ל לא תקינה יש לתקן',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.login.email =  '';
		}	
		else if ($scope.login.password =="")
		{
			$ionicPopup.alert({
			title: 'יש למלא סיסמה',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else
		{
			send_params = 
			{
				"email" : $scope.login.email,
				"password" : $scope.login.password,
				"pushid" : $rootScope.pushId
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/login.php',send_params)
			.success(function(data, status, headers, config)
			{
				if (data.response.status == 1)
				{
					$localStorage.userid = data.response.userid;
					$localStorage.name = data.response.name;
					$localStorage.email = data.response.email;
					$localStorage.phone = data.response.phone;
					$localStorage.image = data.response.image;
					
					
					$localStorage.birth = data.response.birth;
					$localStorage.gender = data.response.gender;
					$localStorage.address = data.response.address;	
					
					$scope.login.email = '';
					$scope.login.password = '';
					
					
					if (!data.response.phone)
						window.location.href = "#/app/updateinfo";				
					else
					  window.location.href = "#/app/main";	
				}
				else
				{
					$ionicPopup.alert({
					title: 'מייל או סיסמה לא תקינים יש לתקן',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });					
				}

			})
			.error(function(data, status, headers, config)
			{

			});
		}	
	}
	
	//register 
	$scope.registerBtn = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		var emailRegex = /\S+@\S+\.\S+/;
		
		if ($scope.register.name =="")
		{
			$ionicPopup.alert({
			title: 'יש למלא שם מלא',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else if ($scope.register.phone =="")
		{
			$ionicPopup.alert({
			title: 'יש למלא מספר טלפון',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		
		else if ($scope.register.email =="")
		{
			$ionicPopup.alert({
			title: 'יש למלא כתובת דוא"ל',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else if (emailRegex.test($scope.register.email) == false)
		{
			$ionicPopup.alert({
			title: 'כתובת דוא"ל לא תקין יש לתקן',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.register.email =  '';
		}	
		
		else if ($scope.register.birth =="")
		{
			$ionicPopup.alert({
			title: 'יש למלא תאריך לידה',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}




		else if ($scope.register.address =="")
		{
			$ionicPopup.alert({
			title: 'יש למלא כתובת',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}


		
		else if ($scope.register.password =="")
		{
			$ionicPopup.alert({
			title: 'יש למלא סיסמה',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else
		{
			
			$scope.birth = dateFilter($scope.register.birth, 'yyyy-MM-dd'),

			send_params = 
			{
				"name" : $scope.register.name,
				"email" : $scope.register.email,
				"phone" : $scope.register.phone,
				"password" : $scope.register.password,
				
				"gender" : $scope.register.gender,
				"birth" : $scope.birth,
				"address" : $scope.register.address.formatted_address,
				//"address" : $scope.register.address,
				"pushid" : $rootScope.pushId
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/register.php',send_params)
			.success(function(data, status, headers, config)
			{
				//alert (data.response.status);
				
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					title: 'מייל כבר בשימוש יש להתחבר',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });
				   
					$scope.register.email = '';
				}
				else if (data.response.status == 1)
				{
					$ionicPopup.alert({
					title: 'טלפון כבר בשימוש יש להזין טלפון אחר',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });		
				   
					$scope.register.phone = '';
				}
				else if (data.response.status == 2)
				{
					$localStorage.userid = data.response.userid;
					$localStorage.name = $scope.register.name;
					$localStorage.email = $scope.register.email;
					$localStorage.phone = $scope.register.phone;

					$localStorage.birth = $scope.birth;
					$localStorage.gender = $scope.register.gender;
					$localStorage.address = $scope.register.address.formatted_address;
					
					$scope.register.name = '';
					$scope.register.email = '';
					$scope.register.password = '';
					
					window.location.href = "#/app/main";	
				}


			})
			.error(function(data, status, headers, config)
			{

			});
		}
	}


  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {


	
	$scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email);

	
      $ionicLoading.hide();
      //$state.go('app.home');
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name,first_name,last_name,locale&access_token=' + authResponse.accessToken, null,
      function (response) {
				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.FaceBookLoginBtn = function() {
    facebookConnectPlugin.getLoginStatus(function(success){
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);


					getFacebookProfileInfo(success.authResponse)
					.then(function(profileInfo) {

							
						$scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email);
							


						//$state.go('app.home');
					}, function(fail){
						// Fail get profile info
						console.log('profile info fail', fail);
					});
				
      } else {
        // If (success.status === 'not_authorized') the user is logged in to Facebook,
				// but has not authenticated your app
        // Else the person is not logged into Facebook,
				// so we're not sure if they are logged into this app or not.

				//console.log('getLoginStatus', success.status);
 
				$ionicLoading.show({
				  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
				});
 

				// Ask the permissions you need. You can learn more about
				// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };

	$scope.showOptions = function() 
	{

		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'יש להזין מספר טלפון האם ברצונך לעשות זה כעת?',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'עדכן מידע אישי',
		type: 'button-positive',
		onTap: function(e) { 
		  $state.go('app.updateinfo');		
		}
	   },
	   	   {
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  $state.go('app.main');		
		}
	   },
	   ]
	  });
	  

	};	

	
  $scope.FacebookLoginFunction = function(id,firstname,lastname,email,gender)
  {
		//$scope.LogOut();

		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		$scope.fullname = firstname+' '+lastname;
		$scope.gender = (gender == "male" ? "0" : "1");
			
			facebook_data = 
			{
				"id" : id,
				"firstname" : firstname,
				"lastname" : lastname,
				"email" : email,
				"fullname" : $scope.fullname,
				"gender" : $scope.gender,
				"pushid" : $rootScope.pushId
			}					
			$http.post($rootScope.Host+'/facebook_connect.php',facebook_data).success(function(data)
			{
			//	if (data.response.userid)
			//	{
					$localStorage.userid = data.response.userid;
					$localStorage.facebookid = id;
					$localStorage.name = $scope.fullname;
					$localStorage.email = email;
					$localStorage.gender = $scope.gender;
					$localStorage.phone = data.response.phone;
					$localStorage.image = 'https://graph.facebook.com/'+id+'/picture?type=large';

					$localStorage.birth = data.response.birth;
					$localStorage.gender = data.response.gender;
					$localStorage.address = data.response.address;				

					if ($localStorage.phone == "" ||  $localStorage.phone == undefined)
					{
						$state.go('app.updateinfo');	
						//$scope.showOptions();
					}

					else
					{
						$state.go('app.main');	
					}

					
			});
		
	  
  }  

})

.controller('MainCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$timeout,$ionicPlatform,$ionicLoading,$cordovaBeacon) {

	$scope.unread = 0 ;
	$scope.sharedcodes = 0;
	$scope.Codes = [];
	//$scope.whatsNewLenth = 0;
	$scope.fields = 
	{
		"search" : ""
	}
	
    

	
	$rootScope.$on('deletedcode', function(event, args) {

	$ionicPlatform.ready(function() 
	{
		
		 $timeout(function() 
		 {
			 cordova.plugins.notification.badge.set(parseInt(args.count));

		}, 300);	

		
	});	
 
	});	

	

	
	$rootScope.$on('sharedcode', function(event, args) {

		// alert (JSON.stringify(args));
		
		if ($localStorage.phone == args.phone)
		{
			 $timeout(function() {
				$scope.sharedcodes++;
				$rootScope.$broadcast('updateBadges');
			}, 300);
		}

	  

	});	
	

	
	
	$scope.getWhatsNew = function()
	{
	    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
			"phone" : $localStorage.phone
		}
		

		$http.post($rootScope.Host+'/get_whatsnew.php',send_params)
		.success(function(data, status, headers, config)
		{
			//$scope.unread = data.length;
			//$rootScope.UnreadJson = data;
			console.log('unread: ', data);
			$scope.unread  = data.length;
			$rootScope.$broadcast('unreadwhatsnew',$scope.unread);

		})
		.error(function(data, status, headers, config)
		{

		});			
	}
	$scope.getWhatsNew();
	
	
	$rootScope.$on('newmessage', function(event, args) {
		
		// alert (JSON.stringify(args));
		
		//alert (args.phone);
		if ($localStorage.phone == args.phone)
		{
			//alert ("ok");
			 $scope.getWhatsNew();
			 $rootScope.$broadcast('unreadwhatsnew',"new");
			 
		}

	  

	});	

	
	$ionicPlatform.on("resume", function(event) {

	$rootScope.$broadcast('newcomment', { phone: $localStorage.phone } );	
	$rootScope.$broadcast('newmessage', { phone: $localStorage.phone } );	
    $rootScope.$broadcast('refreshcodes', { phone: $localStorage.phone } );
	$rootScope.$broadcast('updatebadgecount', { phone: $localStorage.phone } );	
	});	

	
		


	
	$scope.goToWhats = function()
	{
		if ($scope.unread == 0)
		{
			$ionicPopup.alert({
			title: 'אין כרגע הודעות יש לנסות שוב מאוחר יותר',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else
		{
			window.location.href = "#/app/whatsnew";
		}
	}
	
	
	  $rootScope.$on('whatsnew', function(events, args){
	   //$rootScope.UnreadJson = [];
	  
		 $timeout(function() {
			 $scope.getWhatsNew();
		}, 300);
	  })
  
  
  
	$scope.getBadgeCount = function()
	{
		
		//alert ($scope.TypeSelected);
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
			"phone" : $localStorage.phone,
		//	"type" : $scope.TypeSelected,

		}


		
		$http.post($rootScope.Host+'/get_count.php',send_params)
		.success(function(data, status, headers, config)
		{
			console.log("count: ", data);
			
			 $timeout(function() {
				$scope.sharedcodes = data[0].usercodes;
				$scope.qrCountBadge = $scope.sharedcodes;
				$scope.groupsCount = data[0].groups;
				$rootScope.$broadcast('updateBadges',data);
				//alert ($scope.sharedcodes);
			}, 300);
			
		
		})
		.error(function(data, status, headers, config)
		{

		});			
	}

	$scope.getBadgeCount();
	
	
	$scope.scanQR = function()
	{

		//window.location = "#/app/details/scanned_7";
	
	  cordova.plugins.barcodeScanner.scan(
	  function (result) 
	  {
		//  if (result.text)
		//  {
			//alert (result.text);
			
			
			if (result.text.indexOf('http://tapper.co.il/zode/') == 0) {

				$scope.splitText = result.text.split("=");
				$scope.NewText = $scope.splitText[1].replace("zode_", "");;
				$scope.ZodeTitle = $scope.splitText[1].substring(0, 4);

			


			
			if ($scope.ZodeTitle == "zode")
			{
				if ($scope.NewText)
				{
					
					//alert ($scope.splitText[1]);
					
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
					
					send_params = 
					{
						"unique" : $scope.NewText,
						"user" : $localStorage.userid,
						"phone" : $localStorage.phone,
					}
							

					$http.post($rootScope.Host+'/lookup_uniuqe.php',send_params)
					.success(function(data, status, headers, config)
					{
				
						if (data.response.status == 0)
						{
							$ionicPopup.alert({
							title: 'קוד לא נמצא יש לנסות שוב',
							buttons: [{
							text: 'אישור',
							type: 'button-positive',
							  }]
						   });							
						}
						//public or shared code
						else if (data.response.status == 1)
						{
							window.location = "#/app/details/scanned_"+data.response.code;
							//alert ("public or shared code")
							//window.location = "#/app/details/shared_"+data.response.code;
						}
						else if (data.response.status == 2)
						{
							$ionicPopup.alert({
							title: 'הקוד שנסרק הוא פרטי או שלא שותף איתך יש לנסות קוד אחר.',
							buttons: [{
							text: 'אישור',
							type: 'button-positive',
							  }]
						   });							
						}
						// user own code
						else if (data.response.status == 3)
						{
							window.location = "#/app/details/"+data.response.code;
						}
						
					})
					.error(function(data, status, headers, config)
					{

					});						
				}
			}
		}
		else
		{
			if (result.text)
			{
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
				
				send_params = 
				{
					"code" : result.text,
					"user" : $localStorage.userid,
					"phone" : $localStorage.phone,
				}
						

				$http.post($rootScope.Host+'/third_party_scan.php',send_params)
				.success(function(data, status, headers, config)
				{
			
					if (data.response.status == 0)
					{
						
						
					   var confirmPopup = $ionicPopup.confirm({
						 title: 'הקוד שנסרק לא מוכר האם ברצונך להוסיף אותו?',
						 template: ''
					   });
					
					   confirmPopup.then(function(res) {
						if(res) 
						{
							$rootScope.thirdPartyApp = result.text;
							window.location = "#/app/addcode/-1";
							
						} 
						 else 
						 {
						 }
					   });
					}
					else
					{
						window.location = "#/app/details/"+data.response.code;
						//window.location = "#/app/details/shared_"+data.response.code;
					}
					
				})
				.error(function(data, status, headers, config)
				{

				});					
			}

		}

			
			


	  }, 
	  function (error) 
	  {
		  
		 alert (error);
	  }
   );
	}	
	
	$scope.searchBtn = function()
	{
		if ($scope.fields.search =="")
		{
			$ionicPopup.alert({
			title: 'יש למלא חיפוש.',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });				
		}
		else
		{

			//$rootScope.searchText = $scope.fields.search;
			
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});
					
					
			send_params = 
			{
				"search" : $scope.fields.search
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/search.php',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();	
				
				if (data.length > 0)
				{
					$rootScope.searchJson = data;
					window.location = "#/app/search";
				}
				else
				{
					$rootScope.searchJson = [];
					$ionicPopup.alert({
					title: 'לא נמצאו תוצאות יש לנסות שוב.',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });
				}
					
						
				
				console.log("search: " , data);
				$scope.fields.search = "";
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});
	
	
			
			
		}


						   
	}
	
	$scope.enterPress = function(keyEvent)
	{
		if (keyEvent.which === 13)
		{
			$scope.searchBtn();
		}
	}
	
	
})

.controller('CodesCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$ionicScrollDelegate,$ionicModal,$cordovaContacts,$ionicPlatform,$ionicLoading,$cordovaPrinter,ClosePopupService,$timeout) {
	
	$scope.TypeSelected = $rootScope.TypeSelected;
	//alert ($scope.TypeSelected)
	$scope.Codes = [];
	$scope.Scans = [];
	$scope.selectedPhones = [];
	$scope.selectedGroups = [];
	$scope.SharedId = '';
	$scope.ShareUnique = '';
	$scope.ShareIndex = '';
	$scope.ShareType = '';
	$scope.host = $rootScope.Host;
	$scope.SendType = '';
	$scope.scanBtn = 'img/scan2.png';
	$scope.myQr = 'img/myqr2.png';
	$scope.printId = '';
	



	$rootScope.$on('updatedcode', function(event, args) {


			 $timeout(function() 
			 {
				 $scope.getCodes();

			}, 300);	 


	});	

	
	

	
	$rootScope.$on('sharedcode', function(event, args) {

		// alert (JSON.stringify(args));
		
		//alert (args.phone);
		if ($localStorage.phone == args.phone)
		{
			$scope.getCodes();
			$scope.changeType(2);
		}

	  

	});	


	$rootScope.$on('refreshcodes', function(event, args) {


		if ($localStorage.phone == args.phone)
		{
			 $scope.getCodes();

			 //$scope.getNew();
			 //alert (444);
			 //$rootScope.$broadcast('reloadwhatsnew');
		}

	});	

	
	
	$scope.getCodes = function()
	{
		//alert ($scope.TypeSelected);
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
			"phone" : $localStorage.phone,
		//	"type" : $scope.TypeSelected,

		}
		
		if ($scope.TypeSelected == 1)
		{
			$scope.Url = 'get_codes.php';
		}
		else
		{
			$scope.Url = 'get_scanned.php';
		}

		
		$http.post($rootScope.Host+'/'+$scope.Url,send_params)
		.success(function(data, status, headers, config)
		{
			if ($scope.TypeSelected == 1)
			{
				$scope.Codes = [];
				$scope.Codes = data;
				//console.log("Codes" , $scope.Codes)
				$rootScope.CodesArray = $scope.Codes;
			}
			else 
			{
				$scope.Scans = [];
				$scope.Scans = data;
	// 			console.log('scans : ',$scope.Scans)
				$rootScope.CodesArray = $scope.Scans;	
				console.log("Scans: " ,$scope.Scans )
			}
		
		})
		.error(function(data, status, headers, config)
		{

		});			
}

	//$scope.getCodes();

			

	$scope.changeType = function(Type)
	{
		
		if (Type)
		{

			$rootScope.TypeSelected = Type;
			$scope.TypeSelected = Type;
			$scope.getCodes();	
			
			//alert ($rootScope.TypeSelected)
		}

		//alert ($scope.TypeSelected);
		
		if (Type == 1)
		{
			$scope.scanBtn = 'img/scan2.png';
			$scope.myQr = 'img/myqr2.png';
		}
		else
		{
			$scope.scanBtn = 'img/scan1.png';
			$scope.myQr = 'img/myqr1.png';		
		}
		
		//alert ($rootScope.TypeSelected)
		//alert ($scope.TypeSelected)
	}

	$scope.changeType($rootScope.TypeSelected);

	$scope.blurText = function()
	{
		$ionicScrollDelegate.scrollTop();
	}


  
	$scope.printImage = function()
	{

		$scope.PrintURL = 'http://chart.apis.google.com/chart?cht=qr&chs=400x400&chl=http://tapper.co.il/zode/index.php?code=zode_'+$scope.printId+'&chld=H|0';

		if($cordovaPrinter.isAvailable()) {
			$cordovaPrinter.print($scope.PrintURL);
		} else {
			alert("יש נמצאה מדפסת");
		}	

		$scope.closeModal();	  
	}		


	
	$scope.codeOptions = function(type)
	{
		//alert (type);
		//alert ($scope.SharedId)
		//alert ($scope.ShareUnique)
		//alert ($scope.ShareIndex)

		
		if (type == 0)
		{
			$scope.deleteCode($scope.SharedId,$scope.ShareIndex);
		}
		else if (type == 1)
		{
			window.location = "#/app/addcode/"+$scope.SharedId;
		}
		else if (type == 2)
		{
			$scope.openContactsModal(0);
		}
		else if (type == 3)
		{
			$scope.openGroupsModal(1);
		}
		else if (type == 4)
		{
			$scope.shareOptions();
		}
		
	}

	$scope.deleteCode = function(id,index)
	{
		//alert (id);
		//alert (index);
		//return;
	    var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	    });

		if ($scope.TypeSelected == 1)
		{
			$scope.DeleteUrl = 'delete_code.php';
		}
		else
		{
			$scope.DeleteUrl = 'delete_scan.php';
		}
	   
	   
	   //alert (id);
	   //return;
	   //alert ($scope.TypeSelected);
	
	   confirmPopup.then(function(res) {
		if(res) 
		{
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			send_params = 
			{
				"code" : id,
				"phone" : $localStorage.phone,
				"name" : $localStorage.name,
			}
					

			$http.post($rootScope.Host+'/'+$scope.DeleteUrl,send_params)
			.success(function(data, status, headers, config)
			{
				$ionicPopup.alert({
					 title: 'קוד נמחק בהצלחה',
					 template: ''
				   });

				if ($scope.TypeSelected == 1)
				{
					$scope.Codes.splice(index, 1);
				}
				else
				{
					$scope.Scans.splice(index, 1);
				}
   

				//$scope.Appoinments = {};
			})
			.error(function(data, status, headers, config)
			{

			});	
		} 
		 else 
		 {
		 }
	   });
	   
	}



	$scope.openModal= function(newimage)
	{	$scope.printId = newimage;
		$scope.qrimage = 'http://chart.apis.google.com/chart?cht=qr&chs=100x100&chl=http://tapper.co.il/zode/index.php?code=zode_'+newimage+'&chld=H|0';

		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}

	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}


	/** share code  **/

	  
	 $scope.pickContactUsingNativeUI = function () {
		$cordovaContacts.pickContact().then(function (contactPicked) {
		  $scope.contact = contactPicked;
		  //console.log ($scope.contact.phoneNumbers[0].value)
		  
		  $scope.selectedPhones = $scope.contact.phoneNumbers[0].value;
		  
		  $scope.saveShare();
		});
	  }	

  
	  $scope.shareOptions = function()
	{

		window.plugins.socialsharing.share($localStorage.name+' שיתף איתך קוד', 'Zode Code', '', 'https://play.google.com/store/apps/details?id=com.tapper.zode');

		
		/*
		
		
		window.plugins.socialsharing.available(function(isAvailable) {
		  // the boolean is only false on iOS < 6
		  if (isAvailable) {
				window.plugins.socialsharing.share($localStorage.name+' shared a code with you', 'Zode Code', 'http://chart.apis.google.com/chart?cht=qr&chs=100x100&chl=http://tapper.co.il/zode/index.php?code=zode_'+$scope.ShareUnique+'&chld=H|0', 'http://tapper.co.il/zode/index.php?code=zode_'+$scope.ShareUnique),
			  $scope.onSuccess(), // optional success function
			  $scope.onError()   // optional error function
		  }
		});
		*/
	}

	$scope.onSuccess = function()
	{
		/*
		$ionicPopup.alert({
		title: 'code shared successfully',
		buttons: [{
		text: 'אישור',
		type: 'button-positive',
		  }]
	   });
	   */   
	}

	$scope.onError  = function()
	{
		
	}

	$scope.ShareCode = function(id,index,unique,type)
	{
		$scope.SharedId = id;
		$scope.ShareUnique = unique;
		$scope.ShareIndex = index;
		$scope.ShareType = type;

			
		//alert (id);
		//alert (index);
		//alert (unique);
		$ionicModal.fromTemplateUrl('share-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(sharemodal) {
		  $scope.sharemodal = sharemodal;
		  $scope.sharemodal.show();
		 });

		 
	}
	
	$scope.closeShare = function()
	{
		$scope.sharemodal.hide();
	}
	
	
	$scope.ShareCode2= function(id,index,unique)
	{
		
			$scope.SharedId = id;
			$scope.ShareUnique = unique;
		
			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: 'עם מי ברצונך לשתף את הקוד?',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [

		   {
			text: 'אחר',
			type: 'button-royal',
			onTap: function(e) { 
			  $scope.shareOptions();
			}
		   },
		   {
			text: 'קבוצות',
			type: 'button-positive',
			onTap: function(e) { 
			  $scope.openGroupsModal(1);
			}
		   },
		   {
			text: 'אנשי קשר',
			type: 'button-calm',
			onTap: function(e) { 
			 $scope.openContactsModal(0);
			}
		   },
	   
		   /*
			{
			text: 'CANCEL',
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   */
		   
		   ]
		  });
		  
		  ClosePopupService.register(myPopup);
	}


	$scope.openContactsModal = function(type)
	{
			$scope.SendType = type;

		$scope.pickContactUsingNativeUI();

	}


	$scope.openGroupsModal = function(type)
	{
		$scope.SendType = type;
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
		//	"type" : $scope.TypeSelected,

		}
		
		$http.post($rootScope.Host+'/get_groups.php',send_params)
		.success(function(data, status, headers, config)
		{
			$scope.GroupsArray = data;
			if (data.length == "0")
			{
				$ionicPopup.alert({
				title: 'יש ליצור קבוצה תחילה לפני שתוכל לשתף',
				buttons: [{
				text: 'אישור',
				type: 'button-positive',
				  }]
			   });				
			}
			else
			{
			  $ionicModal.fromTemplateUrl('templates/groups_modal.html', {
				scope: $scope
			  }).then(function(GroupsModal) {
				$scope.GroupsModal = GroupsModal;
				$scope.GroupsModal.show();
			  });			   
			}
		
		})
		.error(function(data, status, headers, config)
		{

		});	
		
		

	}

	$scope.closeGroupsModal = function()
	{
		$scope.GroupsModal.hide();
	}


	$scope.closeContacts = function()
	{
		$scope.contactsModal.hide();
	}

	$scope.pushphones = function(phone)
	{
		$scope.selectedPhones.push({
			"phone": phone
		});
		
	}
	$scope.pushGroups = function(id)
	{

		$scope.selectedGroups.push({
			"id": id
		});	
				
	}



	$scope.saveShare = function()
	{
		
		//alert ($scope.SendType);
		console.log($scope.selectedPhones);
		//return;
		
		if ($scope.SendType == 0)
		{
			$scope.ShareUrl = 'share_code.php';
			//$scope.contactsModal.hide();
		}
		else
		{
			$scope.ShareUrl = 'share_group.php';
			$scope.GroupsModal.hide();
		}
		
		//alert ($scope.SharedId);
		console.log('phones: ',$scope.selectedPhones);
		
		
		
		if ($scope.SharedId)
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				send_params = 
				{
					"user" : $localStorage.userid,
					"name" : $localStorage.name,
					"codeid" : $scope.SharedId,
					"phones" : $scope.selectedPhones,
					"groups" : $scope.selectedGroups
				}

				$http.post($rootScope.Host+'/'+$scope.ShareUrl,send_params)
				.success(function(data, status, headers, config)
				{
					
					$ionicPopup.alert({
					title: 'קוד שותף בהצלחה',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });
				   
				
				})
				.error(function(data, status, headers, config)
				{
					$ionicPopup.alert({
					title: 'שגיאה בשיתוף יש לנסות שוב',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });
				});			
		}
	}

	$scope.saveGroups = function()
	{
		console.log($scope.selectedGroups);
		$scope.saveShare(1);
		//$scope.openContactsModal(1);
	}

	/** share code end **/

})

.controller('AddCodeCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$ionicModal,$timeout,$cordovaCamera,ClosePopupService,$ionicLoading,$ionicSlideBoxDelegate,$sce,$sanitize) {
		
	$scope.TypeSelected = 0;
	$scope.CodeId = $stateParams.ItemId;
	$scope.Found = 0;
	$scope.host = $rootScope.Host;
	$scope.imagesArray = [];
	$scope.updateTyped = 0;
	$scope.thirdparty = $rootScope.thirdPartyApp;
	$scope.privateImage = 'img/private.png';
	$scope.publicImage = 'img/public.png';
	$scope.userid = $localStorage.userid;
	$scope.imageNumber = 0;
	$scope.updatedcode = 0;

	$scope.changeType = function(Type)
	{
		//alert (Type);
		$scope.TypeSelected = Type;
		$scope.publicPrivate(Type);
		$scope.updateTyped = 1;
		$scope.type = Type;
	}

	$scope.publicPrivate = function(type)
	{
		if (type == 0)
		{
			$scope.privateImage = 'img/private.png';
			$scope.publicImage = 'img/public.png';
		}
		else
		{
			$scope.privateImage = 'img/private1.png';
			$scope.publicImage = 'img/public1.png';	
		}
	}

//$scope.imagesArray  = ["img/img3.png","img/img2.png","img/img1.png"]

	$scope.fields = 
	{
	 "title" : "", 
	 "desc" : "",
	 "address" : "",
	 "telephone" : "",
	 "fax" : "",
	 "mail" : "",
	 "facebook" : "",
	 "youtube" : "",
	 "twitter" : "",
	 "site" : ""
	}
	$scope.toggles = 
	{
		"address" : false,
		"telephone" : false,
		"fax" : false,
		"mail" : false,
		"facebook" : false,
		"youtube" : false,
		"twitter" : false,
		"site" : false
		
		
	}


	$scope.changeInput = function()
	{
		$scope.updatedcode = 1;
	}

	
	if ($scope.CodeId != -1)
	{
		for(var i=0;i<$rootScope.CodesArray.length; i++)
		{
			if($rootScope.CodesArray[i].index == $scope.CodeId)
			{
				$scope.CodeData = $rootScope.CodesArray[i];
				$scope.Found = 1;
			}
		}
		
		console.log("code data: ", $scope.CodeData)
		
		$scope.codeOwner = $scope.CodeData.user_id;
		
		
		//if ($scope.codeOwner == $localStorage.userid)
			//$scope.isOwner = true;
		//else
			//$scope.isOwner = false;
		
		

		if ($scope.updateTyped == 1)
		{
			$scope.type = $scope.TypeSelected;
		}
		else
		{
			$scope.type = $scope.CodeData.private;
		}
		
		
		$scope.publicPrivate($scope.type);
		
		
	if ($scope.CodeData.images)
	$scope.imagesArray = $scope.CodeData.images;

		//console.log('images : ', $scope.CodeData.images)
		
		
		$scope.CheckToggles = function()
		{
			
			if ($scope.CodeData.toggles.address == 1)
			{
				$scope.addressToggle = 1;
				$scope.toggles.address = true;	
			}			
			if ($scope.CodeData.toggles.facebook == 1)
			{
				$scope.facebookToggle = 1;
				$scope.toggles.facebook = true;
			}				
			if ($scope.CodeData.toggles.fax == 1)
			{
				$scope.faxToggle = 1;
				$scope.toggles.fax = true;
			}				
			if ($scope.CodeData.toggles.mail == 1)
			{
				$scope.mailToggle = 1;
				$scope.toggles.mail = true;
			}			
			if ($scope.CodeData.toggles.site == 1)
			{
				$scope.siteToggle = 1;
				$scope.toggles.site = true;
			}			
			if ($scope.CodeData.toggles.telephone == 1)
			{
				$scope.telephoneToggle = 1;
				$scope.toggles.telephone = true;
			}			
			if ($scope.CodeData.toggles.twitter == 1)
			{
				$scope.twitterToggle = 1;
				$scope.toggles.twitter = true;
			}				
					
			if ($scope.CodeData.toggles.youtube == 1)
			{
				$scope.youtubeToggle = 1;
				$scope.toggles.youtube = true;	
			}
		}
		
		$scope.CheckToggles();
		
	}
	
	else 
	{
		$scope.type = $scope.TypeSelected;
	}
	
	
	//console.log('toggles: ', $scope.CodeData.toggles)
	
	
	if ($scope.Found == 1)
	{
		$scope.fields.title = $scope.CodeData.name;
		$scope.fields.desc = $sce.trustAsHtml($scope.CodeData.details);
		
		$scope.fields.address = $scope.CodeData.address;
		$scope.fields.telephone = $scope.CodeData.phone;
		$scope.fields.fax = $scope.CodeData.fax;
		$scope.fields.mail = $scope.CodeData.email;
		$scope.fields.facebook = $scope.CodeData.facebook;
		$scope.fields.youtube = $scope.CodeData.youtube;
		$scope.fields.twitter = $scope.CodeData.twitter;
		$scope.fields.site = $scope.CodeData.website;
 
	}
	
	
	$scope.rafi = '<ol><li><span style="font-weight: bold; text-decoration: underline; font-style: italic; background-color: rgb(255, 0, 255);">בדיקה 123</span></li></ol>';
	$scope.testing = $sanitize($scope.rafi);

	$scope.deleteImage = function()
	{

		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : $scope.imageNumber

		}
		
		$http.post($rootScope.Host+'/delete_photo.php',send_params)
		.success(function(data, status, headers, config)
		{

			$scope.imagesArray.splice($scope.imageIndex, 1);
			$scope.deleteImageModal.hide();
			$ionicSlideBoxDelegate.update();
			
			//$scope.closeImageModal();

		})
		.error(function(data, status, headers, config)
		{

		});		


	}
			
			
	$scope.saveBtn = function()
	{
		
		var emailRegex = /\S+@\S+\.\S+/;
		var urlregex = /^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/;
			
		if ($scope.fields.address.formatted_address)
		{
			$scope.address = $scope.fields.address.formatted_address;
		}
		else
		{
			$scope.address = $scope.fields.address;
		}

				
			if ($scope.fields.title =="")
			{
				$ionicPopup.alert({
				title: 'יש למלא כותרת',
				buttons: [{
				text: 'אישור',
				type: 'button-positive',
				  }]
			   });		
			}
			
			else if ($scope.address =="" && $scope.addressToggle == "1")
			{
				$ionicPopup.alert({
				title: 'יש למלא כתובת',
				buttons: [{
				text: 'אישור',
				type: 'button-positive',
				  }]
			   });		
			}

			
			//else 
			//{

		  else  if(!/^[0-9]+$/.test($scope.fields.telephone) && $scope.telephoneToggle == "1")
		  {
			$ionicPopup.alert({
			title: 'יש למלא מספר טלפון',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });		
		   
			$scope.fields.telephone =  '';
		   }

		  else if(!/^[0-9]+$/.test($scope.fields.fax) && $scope.faxToggle == "1")
		  {
			$ionicPopup.alert({
			title: 'יש למלא מספר פקס',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });
		   $scope.fields.fax =  '';
		  }



		else if (emailRegex.test($scope.fields.mail) == false && $scope.mailToggle == "1")
		{
			$ionicPopup.alert({
			title: 'כתובת דוא"ל לא תקינה יש לתקן',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.fields.mail =  '';
		}
	//
		else if (urlregex.test($scope.fields.facebook) == false && $scope.facebookToggle == "1")
		{
			$ionicPopup.alert({
			title: 'כתובת פייסבוק לא תקינה יש לתקן',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.fields.facebook =  '';
		}
		else if (urlregex.test($scope.fields.youtube) == false && $scope.youtubeToggle == "1")
		{
			$ionicPopup.alert({
			title: 'כתובת יוטיוב לא תקינה יש לתקן',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.fields.youtube =  '';
		}
		else if (urlregex.test($scope.fields.twitter) == false && $scope.twitterToggle == "1")
		{
			$ionicPopup.alert({
			title: 'כתובת טויטר לא תקינה יש לתקן',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.fields.twitter =  '';
		}
		else if (urlregex.test($scope.fields.site) == false && $scope.siteToggle == "1")
		{
			$ionicPopup.alert({
			title: 'כתובת אתר לא תקין יש לתקן',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.fields.site =  '';
		}			
		else
		{
		
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});				
			
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


				
				send_params = 
				{
					"id" : $scope.CodeId,
					"user" : $localStorage.userid,
					"type" : $scope.type,
					"title" : $scope.fields.title,
					"desc" : $scope.fields.desc,	
					"address" : $scope.address,
					"phone" : $scope.fields.telephone,
					"fax" : $scope.fields.fax,
					"email" : $scope.fields.mail,
					"facebook" : $scope.fields.facebook,
					"youtube" : $scope.fields.youtube,
					"twitter" : $scope.fields.twitter,
					"site" : $scope.fields.site,
					"thirdparty" : $scope.thirdparty,

					"toggleaddress" : $scope.addressToggle,
					"toggletelephone" : $scope.telephoneToggle,
					"togglefax" : $scope.faxToggle,
					"togglemail" : $scope.mailToggle,
					"togglefacebook" : $scope.facebookToggle,
					"toggleyoutube" : $scope.youtubeToggle,
					"toggletwitter" : $scope.twitterToggle,
					"togglesite" : $scope.siteToggle,
					"imagesArray" : $scope.imagesArray,
					
					
					"updated" : $scope.updatedcode
					
					
				}
				
				if ($scope.CodeId == -1)
				{
					$scope.Url = 'add_code.php';
					$scope.TextDesc = 'הוסף';
				}
				else 
				{
					$scope.Url = 'update_code.php';
					$scope.TextDesc = 'עודכן';
				}

				
				$http.post($rootScope.Host+'/'+$scope.Url,send_params)
				.success(function(data, status, headers, config)
				{
					
					$ionicLoading.hide();
					
	
					var updated = $ionicPopup.alert({
						title: 'קוד  ' + $scope.TextDesc+' בהצלחה',
						buttons: []
					   });
	   
					  $timeout(function() 
					  {
						 updated.close(); //close the popup after 3 seconds for some reason
					  }, 2000);
				   
					$rootScope.thirdPartyApp = '';
					$scope.thirdparty = '';
					window.location = "#/app/codes";

				
				})
				.error(function(data, status, headers, config)
				{
					$ionicLoading.hide();
				});					
		}
					
			
			
	
	//	}

				
	}



	$scope.openTogglesModal = function()
	{
		  $ionicModal.fromTemplateUrl('templates/toggles.html', {
			scope: $scope
		  }).then(function(togglesModal) {
			$scope.togglesModal = togglesModal;
			$scope.togglesModal.show();
		  });
	}

	$scope.closeModal = function()
	{
		$scope.togglesModal.hide();
	}

	$scope.saveToggles = function()
	{
		if ($scope.toggles.address)
			$scope.addressToggle = 1;
			else
			$scope.addressToggle = 0;

		if ($scope.toggles.telephone)
			$scope.telephoneToggle = 1;
			else
			$scope.telephoneToggle = 0;

		if ($scope.toggles.fax)
			$scope.faxToggle = 1;
			else
			$scope.faxToggle = 0;

		if ($scope.toggles.mail)
			$scope.mailToggle = 1;
			else
			$scope.mailToggle = 0;

		if ($scope.toggles.facebook)
			$scope.facebookToggle = 1;
			else
			$scope.facebookToggle = 0;

		if ($scope.toggles.youtube)
			$scope.youtubeToggle = 1;
			else
			$scope.youtubeToggle = 0;

		if ($scope.toggles.twitter)
			$scope.twitterToggle = 1;
			else
			$scope.twitterToggle = 0;

		if ($scope.toggles.site)
			$scope.siteToggle = 1;
			else
			$scope.siteToggle = 0;
		
		
		
		$scope.togglesModal.hide();
		
	}

	$scope.deleteCode = function()
	{
		   var confirmPopup = $ionicPopup.confirm({
			 title: 'האם לאשר מחיקה?',
			 template: ''
		   });

		   confirmPopup.then(function(res) {
			if(res) 
			{
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
				
				send_params = 
				{
					"code" : $scope.CodeId
				}
						

				$http.post($rootScope.Host+'/delete_code.php',send_params)
				.success(function(data, status, headers, config)
				{
					$ionicPopup.alert({
						 title: 'קוד נמחק בהצלחה',
						 template: ''
					   });
					   
					   window.location.href = "#/app/codes";	

					//$scope.Appoinments = {};
				})
				.error(function(data, status, headers, config)
				{

				});	
			} 
			 else 
			 {
			 }
		   });
		   
		   
	}

	$scope.addPictures = function()
	{
		
			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: 'בחר מקור התמונה:',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [


		   {
			text: 'מצלמה',
			type: 'button-positive',
			onTap: function(e) { 
			  $scope.takePicture(1);
			}
		   },
		   {
			text: 'גלריית תמונות',
			type: 'button-calm',
			onTap: function(e) { 
			 $scope.takePicture(0);
			}
		   },
		   /*
			{
			text: 'CANCEL',
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   */
		   ]
		  });
		  
		  ClosePopupService.register(myPopup);
	}

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {
				

				$scope.imagesArray.unshift({
					"image": data.response
				});
				
				$scope.image = data.response;
				$scope.uploadedimage = $rootScope.Host+data.response;
				$scope.updatedcode = 1;
				//$scope.showimage = true;

				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }
	


	$scope.openImageModal= function(index,newimage,id)
	{	
		$scope.detailImage = $scope.host+newimage;
		$scope.imageNumber = id;
		$scope.imageIndex = index;
		//alert (id);
		$ionicModal.fromTemplateUrl('image-modal2.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(deleteImageModal) {
		  $scope.deleteImageModal = deleteImageModal;
		  $scope.deleteImageModal.show();
		 });
	}
	
	$scope.closeImageModal = function()
	{
		$scope.deleteImageModal.hide();
	}	

})


.controller('DetailsCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$ionicModal,$ionicSlideBoxDelegate,$timeout,ClosePopupService,$ionicLoading,$cordovaContacts,$ionicPlatform,$sce,$cordovaBeacon) {
	
//$scope.$on('$ionicView.enter', function(e) 
//{
	
	$rootScope.BeaconOnce = 0;
	$rootScope.skip = true; //true;
	$scope.showEdit = false;
	$scope.CodeId = $stateParams.ItemId;
	$scope.newId = $scope.CodeId.substring(0, 7);
	
	if ($scope.newId == "scanned")
	{
		$scope.newCodeId = $scope.CodeId.split("_");
		$scope.CodeId = $scope.newCodeId[1];
		$scope.isScanned = true;
	}
	else
	{
		$scope.CodeId = $scope.CodeId;
		$scope.isScanned = false;
	}
	
	
	
	//alert ($scope.CodeId)
	//$scope.SplitCode = $scope.CodeId.split("shared_");
	$scope.Comments = [];
	$scope.imagesArray = [];
	$scope.host = $rootScope.Host;
	//$scope.imageNumber = '';
	$scope.imageIndex = '';
	$scope.SharedId = '';
	$scope.ShareUnique = '';
	$scope.ShareIndex = '';
	$scope.ShareType = '';

	
	//$ionicSlideBoxDelegate.$getByHandle('image-viewer').update();
	$scope.userid = $localStorage.userid;
	$scope.detailsImage = '';
	$scope.CodeData = [];	
	$scope.CodeNumber = $scope.CodeId;	
	$scope.Doneloading = 0;

	$scope.fields =
	{
		"chatbox" : ""
	}

	/*
	$scope.pusher = $rootScope.pusher;

	var channel = $scope.pusher.subscribe('New_Comment');

	channel.bind($scope.CodeId, function(data) 
	{

	$scope.getNewComments();

	})
	*/
 // $ionicPlatform.ready(function () {
		
	 if (window.cordova) 
	 {
		$cordovaBeacon.stopRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("jaalee", "c974306b-abbf-44f5-ada9-0348ef6ee4b8"));
	 }
	 
//   });

	

		
	$scope.getData = function()
	{
		if ($scope.CodeData.comments)
		$scope.Comments = $scope.CodeData.comments;
	
		if ($scope.CodeData.images)
		$scope.imagesArray = $scope.CodeData.images;
		
		
		$scope.ShareUnique = $scope.CodeData.unique;
		
		//alert ($scope.ShareUnique)
		
		
		$scope.trustedHtml = $sce.trustAsHtml($scope.CodeData.details);



		
		if ($scope.CodeData.toggles)
		{
			$scope.addressToggle = ($scope.CodeData.toggles.address == 1 ? 1 : 0);
			$scope.telephoneToggle = ($scope.CodeData.toggles.telephone == 1 ? 1 : 0);
			$scope.faxToggle = ($scope.CodeData.toggles.fax == 1 ? 1 : 0);
			$scope.mailToggle = ($scope.CodeData.toggles.mail == 1 ? 1 : 0);
			$scope.facebookToggle = ($scope.CodeData.toggles.facebook == 1 ? 1 : 0);
			$scope.youtubeToggle = ($scope.CodeData.toggles.youtube == 1 ? 1 : 0);
			$scope.twitterToggle = ($scope.CodeData.toggles.twitter == 1 ? 1 : 0);
			$scope.siteToggle = ($scope.CodeData.toggles.site == 1 ? 1 : 0);			
		}					
	}
	
	
	
	$scope.getSingleCode = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		$scope.isExsist = 0;
		console.log("Single")
		send_params = 
		{
			//"code" : $scope.SplitCode[1],
			"code" : $scope.CodeId,
			"user" : $localStorage.userid,
			"phone" : $localStorage.phone
		}
		
		for(i=0;i<$rootScope.CodesArray.length;i++)
		{
			if($rootScope.CodesArray[i].index == $scope.CodeId)
			{
				console.log("Exsist")
				$scope.isExsist = 1;
				$timeout(function(){
      				$ionicSlideBoxDelegate.update();
				},100);
				$scope.CodeData = $rootScope.CodesArray[i];
				console.log ('code data : ',$scope.CodeData);
				$scope.Doneloading = 1;
				if ($scope.CodeData.user_id == $localStorage.userid)
					$scope.isOwner = true;
					else
					$scope.isOwner = false;
				
				if ($scope.isOwner)
					$scope.showEdit = true;					
					
				$scope.getData();
			}
		}
		
		
		if($scope.isExsist == 0)
		{
				console.log("NotExsist " ,send_params )
				$http.post($rootScope.Host+'/get_single_code.php',send_params)
				.success(function(data, status, headers, config)
				{
					$timeout(function(){
							$ionicSlideBoxDelegate.update();
					},0);
					$scope.CodeData = data[0];
					console.log ('code data : ',$scope.CodeData);
					$scope.Doneloading = 1;
					if ($scope.CodeData.user_id == $localStorage.userid)
						$scope.isOwner = true;
						else
						$scope.isOwner = false;
					
				if ($scope.isOwner)
					$scope.showEdit = true;					
						
					$scope.getData();
							
				})
				.error(function(data, status, headers, config)
				{
		
				});		
			
		}
			
	}
	
	
		
	
	
	$scope.getSingleCode();
	

	
	
	
		$scope.$watch('CodeData', function () 
		{   
		
			$scope.imagesArray = $scope.CodeData.images;

		});

		
		
		
	/*
	for(var i=0;i<$rootScope.CodesArray.length; i++)
	{
		if($rootScope.CodesArray[i].index == $scope.CodeId)
		{
			$scope.CodeData = $rootScope.CodesArray[i];
		}
	}
	$scope.getData();	
	*/
	
	$scope.deleteComment = function(index,id)
	{
		//alert (index);
		//alert (id);
		
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקת תגובה?',
		 template: ''
	   });

	   
	   //alert ($scope.TypeSelected);
	
	   confirmPopup.then(function(res) {
		if(res) 
		{
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			send_params = 
			{
				"id" : id
			}
					

			$http.post($rootScope.Host+'/delete_comment.php',send_params)
			.success(function(data, status, headers, config)
			{
				
				$scope.Comments.splice(index, 1);
				
			})
			.error(function(data, status, headers, config)
			{

			});	
		} 
		 else 
		 {
		 }
	   });

	   
		
	}
	
	$scope.sendComment = function()
	{
		//$scope.Comments = {};
		
		if ($scope.fields.chatbox)
		{
			
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});			
			
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				if ($localStorage.image)
					$scope.profileimage = $localStorage.image;
				else
					$scope.profileimage = 'img/avatar.png';

				
			
			send_params = 
			{
				"codeid" : $scope.CodeNumber,
				"user" : $localStorage.userid,
				"name" : $localStorage.name,
				"text" : $scope.fields.chatbox,
				"phone" : $localStorage.phone,
				"image" : $scope.profileimage
				
			}
			console.log('params sent comment',  send_params)

			$http.post($rootScope.Host+'/add_comment.php',send_params)
			.success(function(data, status, headers, config)
			{
				console.log('SendParams ',  data)
				$ionicLoading.hide();

				$scope.Comments.unshift({
					"name": $localStorage.name,
					"text": $scope.fields.chatbox,
					"image" :  $scope.profileimage
				});
				
				$scope.fields.chatbox = '';
				
						
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});				
		}
		
	}
			
	$scope.enterPress = function(keyEvent)
	{
		if (keyEvent.which === 13)
		{
			$scope.sendComment();
		}
	}
	
	
	
		
			
	$scope.showImageModal= function(newimage)
	{	
		$scope.detailsImage = $scope.host+newimage;
		//$scope.imageNumber = id;
		//$scope.imageIndex = index;
		
	
		$ionicModal.fromTemplateUrl('details-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(imageDetailsModal) {
		  $scope.imageDetailsModal = imageDetailsModal;
		  $scope.imageDetailsModal.show();
		 });
	}
	
	$scope.closeImageModal = function()
	{
		$scope.imageDetailsModal.hide();
	}
			

		
	$scope.clearReadMessages = function()
	{
	    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
			"phone" : $localStorage.phone,
			"code" : $scope.CodeNumber
		}
		

		$http.post($rootScope.Host+'/clear_read_messages.php',send_params)
		.success(function(data, status, headers, config)
		{
			
		})
		.error(function(data, status, headers, config)
		{

		});				
	}

	$scope.clearReadMessages();
	
	
	$scope.getNewComments = function()
	{
	    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"code" : $scope.CodeNumber
		}
		

		$http.post($rootScope.Host+'/get_new_comments.php',send_params)
		.success(function(data, status, headers, config)
		{
			$scope.Comments = data;
		})
		.error(function(data, status, headers, config)
		{

		});			
	}



	
	
	$scope.openPage = function(URL)
	{
		iabRef = window.open(URL, '_blank', 'location=yes');
	}

	//twitter
	$scope.openPage2 = function(URL)
	{
		iabRef = window.open(URL, '_system', 'location=yes');
	}
	
	//facebook
	$scope.openPage3 = function(URL)
	{
		iabRef = window.open(URL, '_self', 'location=yes');
	}

	
	$scope.openWaze = function(URL)
	{
		//window.location = "waze://?q="+URL+"&navigate=yes";

		iabRef =  window.open('waze://?q='+URL+'&navigate=yes', '_blank', 'location=no');
		//iabRef.close();  

	}
	/*
	 iabRef.addEventListener('exit', iabClose);
	 iabRef.addEventListener('loadstart', iabLoadStart);
	 iabRef.addEventListener('loadstop', iabLoadStop);
	*/
		 
	
	$scope.openMail = function(URL)
	{
		window.location = "mailto:"+URL;
	}

	$scope.openPhone = function(URL)
	{
		 window.location.href="tel://"+URL;
	}	
	/*
	 function iabClose(event) 
	{

         iabRef.removeEventListener('exit', iabClose);
    }
	
    function iabLoadStart(event) 
	{
		iabRef.close();  
    }

    function iabLoadStop(event) 
	{
		
	}
	*/
	
	$scope.addScanned = function(id)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם להוסיף קוד לקודים סרוקים?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			send_params = 
			{
				"id" : id,
				"user" : $localStorage.userid,
				"phone": $localStorage.phone

			}
			
			$http.post($rootScope.Host+'/new_scan.php',send_params)
			.success(function(data, status, headers, config)
			{
				if (data.response.status == 1)
				{
					$ionicPopup.alert({
					title: 'קוד נוסף לסרוקים',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });						
				}
				else
				{
					$ionicPopup.alert({
					title: 'קוד כבר קיים בקודים סרוקים',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });						
				}
				
			
			})
			.error(function(data, status, headers, config)
			{

			});	
			
		} 
		 else 
		 {
		 }
	   });
	}

/** share code  **/

	$scope.codeOptions = function(type)
	{
		//alert (type);
		//alert ($scope.SharedId)
		//alert ($scope.ShareUnique)
		//alert ($scope.ShareIndex)

		
		if (type == 0)
		{
			//$scope.deleteCode($scope.SharedId,$scope.ShareIndex);
		}
		else if (type == 1)
		{
			//alert ($scope.CodeId);
			window.location = "#/app/addcode/"+$scope.CodeId;
		}
		else if (type == 2)
		{
			$scope.openContactsModal(0);
		}
		else if (type == 3)
		{
			$scope.openGroupsModal(1);
		}
		else if (type == 4)
		{
			$scope.shareOptions();
		}
		else if (type == 5)
		{
			//alert ($scope.SharedId);
			$scope.addScanned($scope.SharedId);
		}		
	}
	
	
  
	 $scope.pickContactUsingNativeUI = function () {
		$cordovaContacts.pickContact().then(function (contactPicked) {
		  $scope.contact = contactPicked;
		  //console.log ($scope.contact.phoneNumbers[0].value)
		  
		  $scope.selectedPhones = $scope.contact.phoneNumbers[0].value;
		  
		  $scope.saveShare();
		});
	  }	
 
  
	  $scope.shareOptions = function()
	{
		
		var options = {
		  message: 'share this', // not supported on some apps (Facebook, Instagram)
		  subject: 'the subject', // fi. for email
		  //files: ['', ''], // an array of filenames either locally or remotely
		  url: 'https://www.website.com/foo/#bar?a=b',
		  chooserTitle: 'Pick an app' // Android only, you can override the default share sheet title
		}

		
		window.plugins.socialsharing.share($localStorage.name+' שיתף איתך קוד', 'Zode Code', '', 'https://play.google.com/store/apps/details?id=com.tapper.zode');
		//window.plugins.socialsharing.shareWithOptions(options, $scope.onSuccess(), $scope.onError());

		
		/*
		window.plugins.socialsharing.available(function(isAvailable) {
		  // the boolean is only false on iOS < 6
		  if (isAvailable) {
				window.plugins.socialsharing.share($localStorage.name+' shared a code with you', 'Zode Code', '', ''),
			  $scope.onSuccess(), // optional success function
			  $scope.onError()   // optional error function
		  }
		});
		*/
	}

	$scope.onSuccess = function()
	{
		/*
		$ionicPopup.alert({
		title: 'code shared successfully',
		buttons: [{
		text: 'אישור',
		type: 'button-positive',
		  }]
	   });
	   */   
	}

	$scope.onError  = function()
	{
		
	}

	$scope.ShareCode = function(id,index,unique,type)
	{
		$scope.SharedId = id;
		$scope.ShareUnique = unique;
		$scope.ShareIndex = index;
		$scope.ShareType = type;

			
		//alert (id);
		//alert (index);
		//alert (unique);
		$ionicModal.fromTemplateUrl('share-modal3.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(sharemodal) {
		  $scope.sharemodal = sharemodal;
		  $scope.sharemodal.show();
		 });

		 
	}
	
	$scope.closeShare = function()
	{
		$scope.sharemodal.hide();
	}
	
	
	

	
	
	$scope.ShareCode2= function(id,index,unique)
	{
			$scope.SharedId = id;
			$scope.ShareUnique = unique;
		
			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: 'בחר עם מי תרצה לשתף:',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [

		   {
			text: 'אחר',
			type: 'button-royal',
			onTap: function(e) { 
			  $scope.shareOptions();
			}
		   },
		   {
			text: 'קבוצות',
			type: 'button-positive',
			onTap: function(e) { 
			  $scope.openGroupsModal(1);
			}
		   },
		   {
			text: 'אנשי קשר',
			type: 'button-calm',
			onTap: function(e) { 
			 $scope.openContactsModal(0);
			}
		   },
	   
		   /*
			{
			text: 'CANCEL',
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   */
		   ]
		  });
		  
		 ClosePopupService.register(myPopup);

	}


	$scope.openContactsModal = function(type)
	{
		$scope.SendType = type;

		$scope.pickContactUsingNativeUI();
		//$scope.getContacts();
		
		/*
		  $ionicModal.fromTemplateUrl('templates/contacts.html', {
			scope: $scope
		  }).then(function(contactsModal) {
			$scope.contactsModal = contactsModal;
			$scope.contactsModal.show();
		  });
		*/
	}


	$scope.openGroupsModal = function(type)
	{
		$scope.SendType = type;
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
		//	"type" : $scope.TypeSelected,

		}
		
		$http.post($rootScope.Host+'/get_groups.php',send_params)
		.success(function(data, status, headers, config)
		{
			$scope.GroupsArray = data;
			if (data.length == "0")
			{
				$ionicPopup.alert({
				title: 'יש ליצור קבוצות לפני שתוכל לשתף קבוצה',
				buttons: [{
				text: 'אישור',
				type: 'button-positive',
				  }]
			   });				
			}
			else
			{
			  $ionicModal.fromTemplateUrl('templates/groups_modal.html', {
				scope: $scope
			  }).then(function(GroupsModal) {
				$scope.GroupsModal = GroupsModal;
				$scope.GroupsModal.show();
			  });			   
			}
		
		})
		.error(function(data, status, headers, config)
		{

		});	
		
		

	}

	$scope.closeGroupsModal = function()
	{
		$scope.GroupsModal.hide();
	}


	$scope.closeContacts = function()
	{
		$scope.contactsModal.hide();
	}

	$scope.pushphones = function(phone)
	{
		$scope.selectedPhones.push({
			"phone": phone
		});
		
	}
	$scope.pushGroups = function(id)
	{

		$scope.selectedGroups.push({
			"id": id
		});	
				
	}



	$scope.saveShare = function()
	{
		
		//alert ($scope.SendType);
		console.log($scope.selectedPhones);
		//return;
		
		if ($scope.SendType == 0)
		{
			$scope.ShareUrl = 'share_code.php';
			//$scope.contactsModal.hide();
		}
		else
		{
			$scope.ShareUrl = 'share_group.php';
			$scope.GroupsModal.hide();
		}
		
		//alert ($scope.SharedId);
		console.log('phones: ',$scope.selectedPhones);
		
		
		
		if ($scope.SharedId)
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				send_params = 
				{
					"user" : $localStorage.userid,
					"name" : $localStorage.name,
					"codeid" : $scope.SharedId,
					"phones" : $scope.selectedPhones,
					"groups" : $scope.selectedGroups
				}

				$http.post($rootScope.Host+'/'+$scope.ShareUrl,send_params)
				.success(function(data, status, headers, config)
				{
					
					$ionicPopup.alert({
					title: 'קוד שותף בהצלחה',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });
				   
				
				})
				.error(function(data, status, headers, config)
				{
					$ionicPopup.alert({
					title: 'שגיאה בשיתוף יש לנסות שוב',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });
				});			
		}
	}

	$scope.saveGroups = function()
	{
		console.log($scope.selectedGroups);
		$scope.saveShare(1);
		//$scope.openContactsModal(1);
	}

/** share code end **/



	$scope.checkBadgeCount = function()
	{

	   $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

	
		send_params = 
		{
			"user" : $localStorage.userid,
			"phone" : $localStorage.phone
		}
	
		$http.post($rootScope.Host+'/get_unread.php',send_params)
		.success(function(data, status, headers, config)
		{
			
			
			 $timeout(function() 
			 {
				 cordova.plugins.notification.badge.set(parseInt(data[0].unread));

			}, 300);	
			

		})
		.error(function(data, status, headers, config)
		{

		});		
				
	}


	$ionicPlatform.ready(function() 
	{
		
		 $timeout(function() 
		 {
			$scope.checkBadgeCount();

		}, 300);	

		
	});

	
	


	
	$rootScope.$on('updatebadgecount', function(event, args) {

		$scope.checkBadgeCount();
	});	

	
	$rootScope.$on('newcomment', function(event, args) {
		//alert (JSON.stringify(args));
		$scope.getNewComments();
		$scope.checkBadgeCount();

	});

	
	
	$ionicPlatform.on("resume", function(event) {
		if ($rootScope.State =="app.details")
		{
			$scope.getNewComments();
		    $scope.checkBadgeCount();			
		}

	});	
	

	
	
	
//	})
})


.controller('GroupsCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$ionicModal) {

	$scope.host = $rootScope.Host;


	$scope.getGroups = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
		//	"type" : $scope.TypeSelected,

		}
		

	
	$http.post($rootScope.Host+'/get_groups.php',send_params)
	.success(function(data, status, headers, config)
	{
		$scope.Groups = data;
		console.log('groups: ' , $scope.Groups)
		$rootScope.GroupsArray = $scope.Groups;

	
	})
	.error(function(data, status, headers, config)
	{

	});			
}

	$scope.getGroups();

	$scope.deleteGroup = function(id,index)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקת קבוצה?',
		 template: ''
	   });

	   
	   //alert ($scope.TypeSelected);
	
	   confirmPopup.then(function(res) {
		if(res) 
		{
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			send_params = 
			{
				"user" : $localStorage.userid,
				"group" : id
			}
					

			$http.post($rootScope.Host+'/delete_group.php',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicPopup.alert({
					 title: 'קבוצה נמחקה בהצלחה',
					 template: ''
				   });

				$scope.Groups.splice(index, 1);
			})
			.error(function(data, status, headers, config)
			{

			});	
		} 
		 else 
		 {
		 }
	   });
	}
	
	$scope.openModal= function(newimage)
	{	
	    if (newimage)
		{
			$scope.groupimage = $scope.host+newimage;
			
			$ionicModal.fromTemplateUrl('image-modal.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(modal) {
			  $scope.modal = modal;
			  $scope.modal.show();
			 });			
		}

	}
	
	$scope.closeImageModal = function()
	{
		$scope.modal.hide();
	}

			

})

.controller('AddGroupCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$ionicModal,$timeout,$cordovaCamera,$cordovaContacts,ClosePopupService) {

	$scope.Groupid = $stateParams.ItemId;
	$scope.Contacts = [];
	$scope.selectedContacts = [];
	$scope.host = $rootScope.Host;

	$scope.fields = 
	{
		"title" : "",
		"desc" : "",
		"image" : ""
	}

	$scope.pickContactUsingNativeUI = function () 
	{
		$cordovaContacts.pickContact().then(function (contactPicked) {
		  $scope.contact = contactPicked;
		  //console.log ($scope.contact.phoneNumbers[0].value)
		  
		  $scope.selectedContacts = $scope.contact.phoneNumbers[0].value;
		  //$scope.Contacts.push($scope.contact.name.formatted);
		  
			$scope.Contacts.unshift({
				//"id": id,
				"name" : $scope.contact.name.formatted,
				"phone" : $scope.contact.phoneNumbers[0].value
			});

		
		  //$scope.saveShare();
		});
	}	

		
	if ($scope.Groupid !="-1")
	{
		for(var i=0;i<$rootScope.GroupsArray.length; i++)
		{
			if($rootScope.GroupsArray[i].index == $scope.Groupid)
			{
				$scope.GroupData = $rootScope.GroupsArray[i];
			}
		}
		
		$scope.fields.title = $scope.GroupData.title;
		$scope.fields.desc = $scope.GroupData.desc;
		$scope.fields.image = $scope.GroupData.image;
		
		if ($scope.GroupData.contacts)
			$scope.Contacts = $scope.GroupData.contacts;
		
	}
	
	
	console.log('group data: ',   $scope.GroupData)
		

	$scope.openContactsModal = function()
	{
		  $ionicModal.fromTemplateUrl('templates/contacts.html', {
			scope: $scope
		  }).then(function(contactsModal) {
			$scope.contactsModal = contactsModal;
			$scope.contactsModal.show();
		  });
	}

	$scope.closeContacts = function()
	{
		$scope.contactsModal.hide();
	}

	$scope.onlyUnique = function(value, index, self) { 
		return self.indexOf(value) === index;
	}


	$scope.saveContacts = function()
	{
		//$scope.selectedContacts = $scope.selectedContacts.filter( $scope.onlyUnique ); 
		
		console.log($scope.selectedContacts)
		
		$scope.contactsModal.hide();
	}




	$scope.pushcontacts = function(id,name,phone)
	{
		$scope.selectedContacts.push({
			"id": id,
			"name" : name,
			"phone" : phone
		});
		
	}



	$scope.saveGroup = function()
	{
		//alert ($scope.Contacts);
		//console.log($scope.Contacts);
		//return;
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		if ($scope.Groupid == -1)
		{
			$scope.Url = 'add_group.php';
			$scope.TextDesc = 'נוספה';
		}
		else 
		{
			$scope.Url = 'update_group.php';
			$scope.TextDesc = 'עודכנה';
		}
		if ($scope.fields.title =="")
		{
			$ionicPopup.alert({
			title: 'יש למלא כותרת',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });			
		}
		else
		{
			
				send_params = 
				{
					"id" : $scope.Groupid,
					"user" : $localStorage.userid,
					"title" : $scope.fields.title,
					"desc" : $scope.fields.desc,
					"image" : $scope.image,
					"contacts" : $scope.Contacts
					//"contacts" : $scope.selectedContacts
					
					
				}
				
				console.log('contacts: ' , $scope.Contacts)

				
			$http.post($rootScope.Host+'/'+$scope.Url,send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicPopup.alert({
				title: 'קבוצה  ' + $scope.TextDesc+ 'בהצלחה',
				buttons: [{
				text: 'אישור',
				type: 'button-positive',
				  }]
			   });
			   
			   $scope.fields.title = '';
			   $scope.fields.desc = '';
			   $scope.selectedContacts = [];
			   
				window.location = "#/app/groups";

			
			})
			.error(function(data, status, headers, config)
			{

			});			
		}
	}

	$scope.addPictures = function()
	{
			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: 'יש לבחור מקור התמונה:',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [


		   {
			text: 'מצלמה',
			type: 'button-positive',
			onTap: function(e) { 
			  $scope.takePicture(1);
			}
		   },
		   {
			text: 'גלריית תמונות',
			type: 'button-calm',
			onTap: function(e) { 
			 $scope.takePicture(0);
			}
		   },
		   /*
			   {
			text: 'CANCEL',
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   */
		   ]
		  });
		  
		  ClosePopupService.register(myPopup);
	}

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {
				
	
				$scope.image = data.response;
				$scope.uploadedimage = $rootScope.Host+data.response;
				$scope.fields.image = $scope.image;
				//$scope.showimage = true;

				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }
	
	
	$scope.deleteGroup = function()
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקת קבוצה?',
		 template: ''
	   });

	   
	   //alert ($scope.TypeSelected);
	
	   confirmPopup.then(function(res) {
		if(res) 
		{
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
			send_params = 
			{
				"user" : $localStorage.userid,
				"group" : $scope.Groupid
			}
					

			$http.post($rootScope.Host+'/delete_group.php',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicPopup.alert({
					 title: 'קבוצה נמחקה בהצלחה',
					 template: ''
				   });
				
				window.location.href = "#/app/groups";	
				
				//$scope.Groups.splice(index, 1);
			})
			.error(function(data, status, headers, config)
			{

			});	
		} 
		 else 
		 {
		 }
	   });
	}
	
	$scope.deleteContact = function(index,id)
	{
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : id
		}
				

		$http.post($rootScope.Host+'/delete_contact.php',send_params)
		.success(function(data, status, headers, config)
		{
			$scope.Contacts.splice(index, 1);
		})
		.error(function(data, status, headers, config)
		{

		});	
		
	}


})



.controller('ForgotCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$ionicModal,$timeout,$cordovaCamera,$cordovaContacts) {

	$scope.forgot = 
	{
		"email" : ""
	}

	
$scope.sendPass = function()
{
	
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	var emailRegex = /\S+@\S+\.\S+/;
	
	if ($scope.forgot.email =="")
	{
		$ionicPopup.alert({
		title: 'יש למלא כתובת מייל',
		buttons: [{
		text: 'אישור',
		type: 'button-positive',
		  }]
	   });			
	}
	else if (emailRegex.test($scope.forgot.email) == false)
	{
		$ionicPopup.alert({
		title: 'כתובת מייל לא תקינה יש לתקן',
		buttons: [{
		text: 'אישור',
		type: 'button-positive',
		  }]
	   });	

	   $scope.forgot.email =  '';
	}
	else
	{


	 $timeout(function() 
	 {
		send_params = 
		{
			"email" : $scope.forgot.email
		}
		
		//alert ($scope.forgot.email)
		$http.post($rootScope.Host+'/forgot_password.php',send_params)
		.success(function(data, status, headers, config)
		{
			//alert ("ok");
				if (data.response.status == 1)
				{
					$ionicPopup.alert({
					title: 'סיסמה נשלחה למייל',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });	

					window.location.href = "#/app/login";	
					$scope.forgot.email =  '';
				}
				else
				{
					$ionicPopup.alert({
					title: 'מייל לא נמצא יש לנסות שוב',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });		

					$scope.forgot.email =  '';	
				}


		})
		.error(function(data, status, headers, config)
		{

		});		
		

	}, 300);
			
			

	}
}	
	
})	


.controller('updateInfoCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$ionicModal,$timeout,$cordovaCamera,$cordovaContacts,ClosePopupService,$ionicSideMenuDelegate) {


	$scope.host = $rootScope.Host;
	$scope.userimage = $localStorage.image;
	$ionicSideMenuDelegate.canDragContent(false);

	$scope.newimage = $scope.userimage.substring(0,1);	
	
	//alert ($localStorage.userid);
	
	
	if ($scope.userimage == "")
	{
		$scope.showimage = "img/avatar.png";
	}
	else
	{
		if ($scope.newimage == "u")
		{
			$scope.showimage = $scope.host+$scope.userimage;
		}
		else
		{
			$scope.showimage = $scope.userimage;
		}		
	}
	
	//alert ($scope.showimage);

	
	//alert ($scope.showimage)
	
	$scope.info = 
	{
		"phone" : $localStorage.phone,
		"name" : $localStorage.name,
		
		"birth" : $localStorage.birth,
		"address" : $localStorage.address,
		"gender" : $localStorage.gender,
	}
	
	


			

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) 
				{
					$scope.image = data.response;
					$scope.showimage = $rootScope.Host+data.response;
					//$scope.showimage = true;

				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }

	
	$scope.selectImage = function()
	{
			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: 'יש לבחור מקור התמונה:',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [


		   {
			text: 'מצלמה',
			type: 'button-positive',
			onTap: function(e) { 
			  $scope.takePicture(1);
			}
		   },
		   {
			text: 'גלריית תמונות',
			type: 'button-calm',
			onTap: function(e) { 
			 $scope.takePicture(0);
			}
		   },
		   /*
			   {
			text: 'CANCEL',
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   */
		   ]
		  });
		  
		  ClosePopupService.register(myPopup);
	}	

	$scope.saveInfo = function()
	{
		//if ($scope.info.phone)
		//{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			if ($scope.image)
			{
				$scope.newimage = $scope.image;
				
			}
			else
			{
				$scope.newimage = $scope.userimage;
			}
			
		if (!$scope.info.phone)
		{
			$ionicPopup.alert({
			title: 'יש למלא מספר טלפון',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	
		}
		else
		{
			
			send_params = 
			{
				"user" : $localStorage.userid,
				"phone" : $scope.info.phone,
				"image" : $scope.newimage,
				
				"name" : $scope.info.name,
				"birth" : $scope.info.birth,
				"address" : $scope.info.address,
				"gender" : $scope.info.gender,
			}



		
			
			$http.post($rootScope.Host+'/update_phone.php',send_params)
			.success(function(data, status, headers, config)
			{
				if (data.response.status == 1)
				{
					$ionicPopup.alert({
					title: 'טלפון כבר בשימוש יש למלא טלפון אחר',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });		

					$scope.info.phone = '';
				}
				else
				{
					$localStorage.phone = $scope.info.phone;
					$localStorage.image = $scope.newimage;
					
					$localStorage.name = $scope.info.name;
					$localStorage.birth = $scope.info.birth;
					$localStorage.address = $scope.info.address;
					$localStorage.gender = $scope.info.gender;
	

					$ionicPopup.alert({
					title: 'עודכן בהצלחה',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });	

					window.location.href = "#/app/main";
				}

				

			})
			.error(function(data, status, headers, config)
			{

			});				
		}
			
			
		//}
		//else
		//{
		//	window.location.href = "#/app/main";
		//}	
}

})	


.controller('WhatsNewCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$ionicModal,$timeout,$cordovaCamera,$cordovaContacts) 
{


	$rootScope.$on('newmessage', function(event, args) {

		// alert (JSON.stringify(args));
		
		//alert (args.phone);
		if ($localStorage.phone == args.phone)
		{
			 $timeout(function() 
			 {
				 $scope.getWhatsNew();

			}, 300);	 
		}

	  

	});	

	
	$scope.getWhatsNew = function()
	{
	    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
			"phone" : $localStorage.phone
		}
		

		$http.post($rootScope.Host+'/get_whatsnew.php',send_params)
		.success(function(data, status, headers, config)
		{
			$scope.readMsg = 0;
			$scope.WhatsnewJson = data;
			
			if ($scope.WhatsnewJson.length == 0)
				window.location.href = "#/app/main";
			
			
			console.log("whatsnew : " , $scope.WhatsnewJson)
			for (var i = 0; i < $scope.WhatsnewJson.length; i++) 
			{
				$scope.articleId = $scope.WhatsnewJson[i].id;
				//if ($scope.WhatsnewJson[i].read == 0)
				//{
				//	$scope.WhatsnewJson[i].read = $scope.WhatsnewJson[i].read;
				//}
				//alert ($scope.articleId)
				$scope.WhatsnewJson[i].isOpen = 0;
			}
			
		})
		.error(function(data, status, headers, config)
		{

		});			
	}
	$scope.getWhatsNew();

	
	
	//$scope.$on('$ionicView.enter', function(e) 
	//{
		//$timeout(function() 
		//{

			//$scope.navigateCode = function(comment,item)
			$scope.navigateCode = function(item,index)
			{
				$scope.WhatsnewJson[index].read = 1;
				item.read = 1;
				//alert (item.code_id)
				//alert (id);
				//comment.read = 1;
				//$scope.id = comment.code_id;
				 $rootScope.$broadcast('whatsnew', '');
				 



  
				window.location = "#/app/details/"+item.code_id;

			}
			
			$scope.deleteBtn = function(index,id,deleteid)
			{
			
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

					for (var i = 0; i < $scope.WhatsnewJson.length; i++) 
					{
						if ($scope.WhatsnewJson[i].id == id)
						{
							$scope.newArray = $scope.WhatsnewJson[i].message;
							$scope.newArray.splice(index, 1);
						}
					
					}
				
					send_params = 
					{
						"index" : deleteid
					}
					//console.log(login_params)
					$http.post($rootScope.Host+'/delete_whatsnew.php',send_params)
					.success(function(data, status, headers, config)
					{


					})
					.error(function(data, status, headers, config)
					{

					});
				
			}
			
			$scope.openComment = function(index,item)
			{
				//$rootScope.UnreadJson[index].read = 1;
				
				
				if ($scope.WhatsnewJson[index].isOpen == 1)
					$scope.WhatsnewJson[index].isOpen = 0;
				else
					$scope.WhatsnewJson[index].isOpen = 1;
					
				console.log($scope.WhatsnewJson)
			}
			
	//	}, 300);
	// });


})	

.controller('AboutCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$ionicModal,$timeout,$cordovaCamera,$cordovaContacts) 
{
	
	$rootScope.$watch('SettingsArray', function () 
	{   

		$scope.AboutText = $rootScope.SettingsArray[0].about_text;

	});	
	

})	


.controller('SearchCtrl', function($scope, $stateParams,$localStorage,$http,$ionicPopup,$rootScope,$ionicModal,$timeout,$cordovaCamera,$cordovaContacts,$ionicScrollDelegate,$ionicLoading,ClosePopupService) 
{
	
	
	$scope.SharedId = '';
	$scope.ShareUnique = '';
	$scope.ShareIndex = '';
	$scope.ShareType = '';
	
	
	$scope.fields = 
	{
		"search" : ""
	}
	
	
	
	

	$scope.blurText = function()
	{
		$ionicScrollDelegate.scrollTop();
	}	
	
	


	
	$scope.openModal= function(newimage)
	{	$scope.printId = newimage;
		$scope.qrimage = 'http://chart.apis.google.com/chart?cht=qr&chs=100x100&chl=http://tapper.co.il/zode/index.php?code=zode_'+newimage+'&chld=H|0';

		$ionicModal.fromTemplateUrl('image-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(modal) {
		  $scope.modal = modal;
		  $scope.modal.show();
		 });
	}

	$scope.closeModal = function()
	{
		$scope.modal.hide();
	}

	
	$scope.addScanned = function(id)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם להוסיף קוד לקודים סרוקים?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			send_params = 
			{
				"id" : id,
				"user" : $localStorage.userid,
				"phone": $localStorage.phone

			}
			
			$http.post($rootScope.Host+'/new_scan.php',send_params)
			.success(function(data, status, headers, config)
			{
				if (data.response.status == 1)
				{
					$ionicPopup.alert({
					title: 'קוד נוסף לקודים סרוקים',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });						
				}
				else
				{
					$ionicPopup.alert({
					title: 'קוד קיים כבר בקודים סרוקים',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });						
				}
				
			
			})
			.error(function(data, status, headers, config)
			{

			});	
			
		} 
		 else 
		 {
		 }
	   });
	}

	
	
	$scope.searchBtn = function()
	{
		if ($scope.fields.search =="")
		{
			$ionicPopup.alert({
			title: 'יש למלא חיפוש.',
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });				
		}
		else
		{

			//$rootScope.searchText = $scope.fields.search;
			
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});
					
					
			send_params = 
			{
				"search" : $scope.fields.search
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/search.php',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();	
				
				if (data.length > 0)
				{
					$rootScope.searchJson = data;
					//window.location = "#/app/search";
				}
				else
				{
					$rootScope.searchJson = [];
					$ionicPopup.alert({
					title: 'לא נמצאו תוצאות יש לנסות שוב.',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });
				}
					
						
				
				console.log("search: " , data);
				$scope.fields.search = "";
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});
	
	
			
			
		}


						   
	}
	
	$scope.enterPress = function(keyEvent)
	{
		if (keyEvent.which === 13)
		{
			$scope.searchBtn();
		}
	}

	
	
	/** share code  **/


	$scope.codeOptions = function(type)
	{
		//alert (type);
		//alert ($scope.SharedId)
		//alert ($scope.ShareUnique)
		//alert ($scope.ShareIndex)

		
		if (type == 0)
		{
			$scope.deleteCode($scope.SharedId,$scope.ShareIndex);
		}
		else if (type == 1)
		{
			window.location = "#/app/addcode/"+$scope.SharedId;
		}
		else if (type == 2)
		{
			$scope.openContactsModal(0);
		}
		else if (type == 3)
		{
			$scope.openGroupsModal(1);
		}
		else if (type == 4)
		{
			$scope.shareOptions();
		}
		else if (type == 5)
		{
			//alert ($scope.SharedId);
			$scope.addScanned($scope.SharedId);
		}
		
	}

	
	 $scope.pickContactUsingNativeUI = function () {
		$cordovaContacts.pickContact().then(function (contactPicked) {
		  $scope.contact = contactPicked;
		  //console.log ($scope.contact.phoneNumbers[0].value)
		  
		  $scope.selectedPhones = $scope.contact.phoneNumbers[0].value;
		  
		  $scope.saveShare();
		});
	  }	

  
	  $scope.shareOptions = function()
	{
		
		window.plugins.socialsharing.share($localStorage.name+' שיתף איתך קוד', 'Zode Code', '', 'https://play.google.com/store/apps/details?id=com.tapper.zode');
		
		/*
		window.plugins.socialsharing.available(function(isAvailable) {
		  // the boolean is only false on iOS < 6
		  if (isAvailable) {
				window.plugins.socialsharing.share($localStorage.name+' shared a code with you', 'Zode Code', 'http://chart.apis.google.com/chart?cht=qr&chs=100x100&chl=http://tapper.co.il/zode/index.php?code=zode_'+$scope.ShareUnique+'&chld=H|0', 'http://tapper.co.il/zode/index.php?code=zode_'+$scope.ShareUnique),
			  $scope.onSuccess(), // optional success function
			  $scope.onError()   // optional error function
		  }
		});
		*/
	}

	$scope.onSuccess = function()
	{
		/*
		$ionicPopup.alert({
		title: 'code shared successfully',
		buttons: [{
		text: 'אישור',
		type: 'button-positive',
		  }]
	   });
	   */   
	}

	$scope.onError  = function()
	{
		
	}

	$scope.ShareCode = function(id,index,unique,type)
	{
		$scope.SharedId = id;
		$scope.ShareUnique = unique;
		$scope.ShareIndex = index;
		$scope.ShareType = type;

			
		//alert (id);
		//alert (index);
		//alert (unique);
		$ionicModal.fromTemplateUrl('share-modal2.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(sharemodal) {
		  $scope.sharemodal = sharemodal;
		  $scope.sharemodal.show();
		 });

		 
	}
	
	$scope.closeShare = function()
	{
		$scope.sharemodal.hide();
	}
	
	
	

	
	
	$scope.ShareCode2= function(id,index,unique)
	{
		
			$scope.SharedId = id;
			$scope.ShareUnique = unique;
		
			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: 'בחר אמצעי שיתוף:',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [

		   {
			text: 'אחר',
			type: 'button-royal',
			onTap: function(e) { 
			  $scope.shareOptions();
			}
		   },
		   {
			text: 'קבוצות',
			type: 'button-positive',
			onTap: function(e) { 
			  $scope.openGroupsModal(1);
			}
		   },
		   {
			text: 'אנשי קשר',
			type: 'button-calm',
			onTap: function(e) { 
			 $scope.openContactsModal(0);
			}
		   },
	   
		   /*
			{
			text: 'CANCEL',
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   */
		   
		   ]
		  });
		  
		  ClosePopupService.register(myPopup);
	}


	$scope.openContactsModal = function(type)
	{
			$scope.SendType = type;

		$scope.pickContactUsingNativeUI();

	}


	$scope.openGroupsModal = function(type)
	{
		$scope.SendType = type;
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
		//	"type" : $scope.TypeSelected,

		}
		
		$http.post($rootScope.Host+'/get_groups.php',send_params)
		.success(function(data, status, headers, config)
		{
			$scope.GroupsArray = data;
			if (data.length == "0")
			{
				$ionicPopup.alert({
				title: 'יש ליצור קבוצות תחילה',
				buttons: [{
				text: 'אישור',
				type: 'button-positive',
				  }]
			   });				
			}
			else
			{
			  $ionicModal.fromTemplateUrl('templates/groups_modal.html', {
				scope: $scope
			  }).then(function(GroupsModal) {
				$scope.GroupsModal = GroupsModal;
				$scope.GroupsModal.show();
			  });			   
			}
		
		})
		.error(function(data, status, headers, config)
		{

		});	
		
		

	}

	$scope.closeGroupsModal = function()
	{
		$scope.GroupsModal.hide();
	}


	$scope.closeContacts = function()
	{
		$scope.contactsModal.hide();
	}

	$scope.pushphones = function(phone)
	{
		$scope.selectedPhones.push({
			"phone": phone
		});
		
	}
	$scope.pushGroups = function(id)
	{

		$scope.selectedGroups.push({
			"id": id
		});	
				
	}



	$scope.saveShare = function()
	{
		
		//alert ($scope.SendType);
		console.log($scope.selectedPhones);
		//return;
		
		if ($scope.SendType == 0)
		{
			$scope.ShareUrl = 'share_code.php';
			//$scope.contactsModal.hide();
		}
		else
		{
			$scope.ShareUrl = 'share_group.php';
			$scope.GroupsModal.hide();
		}
		
		//alert ($scope.SharedId);
		console.log('phones: ',$scope.selectedPhones);
		
		
		
		if ($scope.SharedId)
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				send_params = 
				{
					"user" : $localStorage.userid,
					"name" : $localStorage.name,
					"codeid" : $scope.SharedId,
					"phones" : $scope.selectedPhones,
					"groups" : $scope.selectedGroups
				}

				$http.post($rootScope.Host+'/'+$scope.ShareUrl,send_params)
				.success(function(data, status, headers, config)
				{
					
					$ionicPopup.alert({
					title: 'קוד שותף בהצלחה',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });
				   
				
				})
				.error(function(data, status, headers, config)
				{
					$ionicPopup.alert({
					title: 'שגיאה יש לנסות שוב',
					buttons: [{
					text: 'אישור',
					type: 'button-positive',
					  }]
				   });
				});			
		}
	}

	$scope.saveGroups = function()
	{
		console.log($scope.selectedGroups);
		$scope.saveShare(1);
		//$scope.openContactsModal(1);
	}

	/** share code end **/
	
})		

.filter('cutString_TitlePage', function () {
    return function (value, wordwise, max, tail) 
	{
		value =  value.replace(/(<([^>]+)>)/ig,"");
        if (!value) return '';
		
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
    //    if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
     //   }
        return value + (tail || ' ');
    };
})


.filter('commentsImage', function ($rootScope) {
    return function (value) 
	{
		var Img = "";
		
		if(value)
		{
			if (value.substring(0,1) =="u")
			{
				Img = $rootScope.Host+value;
			}
			else
			{
				Img = value;
			}
			///Img = value;
		}
		else
		{
			Img = 'img/avatar.png';
		}
		return Img;
    };
})

.filter('checkImgUrl', function ($rootScope) {
    return function (value) 
	{
		if (value)
		{
			return $rootScope.Host+value;
		}
		else
		{
			return 'img/groupIcon.png';
		}
    };
})



.filter('stripslashes', function () {
    return function (value) 
	{
	    return (value + '')
		.replace(/\\(.?)/g, function(s, n1) {
		  switch (n1) {
			case '\\':
			  return '\\';
			case '0':
			  return '\u0000';
			case '':
			  return '';
			default:
			  return n1;
		  }
		});
    };
})


.factory('ClosePopupService', function($document, $ionicPopup, $timeout){
  var lastPopup;
  return {
    register: function(popup) {
      $timeout(function(){
        var element = $ionicPopup._popupStack.length>0 ? $ionicPopup._popupStack[0].element : null;
        if(!element || !popup || !popup.close) return;
        element = element && element.children ? angular.element(element.children()[0]) : null;
        lastPopup  = popup;
        var insideClickHandler = function(event){
          event.stopPropagation();
        };
        var outsideHandler = function() {
          popup.close();
        };
        element.on('click', insideClickHandler);
        $document.on('click', outsideHandler);
        popup.then(function(){
          lastPopup = null;
          element.off('click', insideClickHandler);
          $document.off('click', outsideHandler);
        });
      });
    },
    closeActivePopup: function(){
      if(lastPopup) {
        $timeout(lastPopup.close);
        return lastPopup;
      }
    }
  };
})


.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

